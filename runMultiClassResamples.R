rm(list = ls())
gc()
library(argparse)
library(feather)
library(caret)
library(HandTill2001)

Matt_Coef <- function (conf_matrix)
{
  TP <- conf_matrix$table[1,1]
  TN <- conf_matrix$table[2,2]
  FP <- conf_matrix$table[1,2]
  FN <- conf_matrix$table[2,1]
  
  mcc_num <- (TP*TN - FP*FN)
  mcc_den <- 
    as.double((TP+FP))*as.double((TP+FN))*as.double((TN+FP))*as.double((TN+FN))
  
  mcc_final <- mcc_num/sqrt(mcc_den)
  return(mcc_final)
}


metricSummary <- function(data,lev=NULL,model=NULL) {
  cmt = confusionMatrix(data[,"pred"],data[,"obs"],positive=NULL)
  f1 = tryCatch(mean(cmt$byClass[,"F1"]), error=function(e) mean(cmt$byClass["F1"]))
  mcc = Matt_Coef(cmt)
  bacc = tryCatch(mean(cmt$byClass[,"Balanced Accuracy"]), error=function(e) mean(cmt$byClass["Balanced Accuracy"]))
  out = c(bacc,mcc,f1)
  names(out) = c("Bal.Acc","MCC","F1")
  out
}


cm_metric <- function(cmt,metric) {
  bacc = tryCatch(mean(cmt$byClass[,metric]), error=function(e) mean(cmt$byClass[metric]))
}
######
# --data_dir: directory where data is saved (either .csv or .RData file)
# --use_csv:  use csv as input. If so, --resample_name and --label_name should be set
# --no_use_csv: do not use csv as input. Use .RData as input. If --no-use-csv, then --label_name and --resample_name do not need to be set
# --label_name: name of file with labels that are interested to classify. Should be located in data_dir
# --sample_group: information on samples. To be used with --use_csv. Should be located in data_dir
# --resample_name: information on train/test splits, each column being a split of the data. TO be used with --use_csv. Located in data_dir
# --data_name: name of input data. If --use_csv, should be a .csv file with all data (both train and test). If --no_use_csv, it should be
#       an RData file with the following variables: xtrain_data, xtest_data, ytrain_data, ytest_data
# --out_dir: Where output files should be saved. Note: will not create directory if it doesn't exist 
# --save_name: name of output .RDS files
# --subset_methods: which of the 4 machine learning methods to use. Select 1 for LASSO, 2 for Ridge, 3 for SVM, 4 for RF
# --k: To be used when have multiple train_cut splits: the index of the train/test split to use
# --f_prefix: a file prefix to append to save_name

# Sample usage: Rscript runMultiClassResamples --data_name resample_input_%d.RData --no_use_csv --save_name resample_out --k 1 --subset_methods 1
######
parser <- ArgumentParser(description = 'Process inputs')
parser$add_argument('--data_dir',default='/home/ec2-user/COVID-19/Data/')
parser$add_argument('--use_csv',action='store_true',dest='useCSV',default=F)
parser$add_argument('--no_use_csv',action='store_false',dest='useCSV',default=F)
parser$add_argument('--label_name',default='')
parser$add_argument('--resample_name',default='resample.csv')
parser$add_argument('--data_name',default='resample.RData')
parser$add_argument('--save_name',default='out')
parser$add_argument('--out_dir',default='/home/ec2-user/COVID-19/Results/')
parser$add_argument('--subset_methods',type='integer',nargs='+',default=1:5)
parser$add_argument('--k',type='integer',default=1)
parser$add_argument('--f_prefix',default='')
parser$add_argument('--save_model',action='store_true',dest="saveModel",default=F)
args = commandArgs(trailingOnly=TRUE)

args = parser$parse_args(args)
data_dir = args$data_dir

data_name = args$data_name
resample_name = args$resample_name
#data_name='69_samples_oxygen_vs_ARDS_resample_1.RData'
label_name = args$label_name 
out_dir = args$out_dir
save_name = args$save_name
# TrainTest_PCAmatrix_%d.RData
# RIN_gte5.5_adjCounts_noRace_nodup_input_%d_megena_scaledPCData.RData

useFibrosis=args$useFibrosis
useCSV = args$useCSV
#useCSV = F
f_prefix = args$f_prefix

k = args$k 
#k=1
methods = c('glmnet','glmnet','svmLinear2','rf','xgbTree')
method_disp = c('lasso','ridge','svmLinear2','rf','xgboost')
metric='Bal.Acc'
lassoGrid=expand.grid(.alpha=1, .lambda=2^(seq(-8,2, by = 0.25)))
ridgeGrid=expand.grid(.alpha=0, .lambda=2^(seq(-8,2, by = 0.25)))
#svmRBFGrid = expand.grid(C=2^seq(-2,3),sigma=2^seq(-17,-12))
svmLinearGrid = expand.grid(.cost=2^seq(-2,3))
rfGrid=expand.grid(.mtry=c(5:30))#, .ntree=c(1000, 1500, 2000))
xgbGrid <- NULL #expand.grid(nrounds = c(100,200))
                      #   max_depth = c(10, 15, 20, 25),
                      #   colsample_bytree = seq(0.5, 0.9, length.out = 5),
                         ## The values below are default values in the sklearn-api. 
                      #   eta = 0.1,
                      #   gamma=0,
                      #   min_child_weight = 1,
                      #   subsample = 1
#  )

tuneGrids=list(lassoGrid,ridgeGrid,svmLinearGrid,rfGrid,xgbGrid)
cvCtrl = trainControl(method = "cv", number = 10,classProbs = TRUE, allowParallel = TRUE,summaryFunction=metricSummary)

subset_methods = args$subset_methods
#subset_methods= 5
response_train_wtl_list = vector("list",length(subset_methods))
response_test_wtl_list = vector("list",length(subset_methods))
cm_train_tables = vector("list",length(subset_methods))
cm_test_tables = vector("list",length(subset_methods))

info = data.frame(dataset=character(),datapart=character(),method=character(),
'Accuracy' = double(),'Bal.Acc'=double(),AUROC=double(),"F1"=double(),stringsAsFactors=F)
count=1

if (useCSV) {    
    print(paste("Running",data_name,"Resample",k))
    data = read.csv(paste(data_dir,data_name,sep=''),check.names=F,stringsAsFactors=F,row.names=1)
    sampleIDs = colnames(data)
    geneIDs = rownames(data) 
    data = as.data.frame(t(data))
    
    labels = read.table(paste(data_dir,label_name,sep=''))
    train_idxs = read.table(paste(data_dir,resample_name,sep=''),header=T,sep=',')
    trainIdx = train_idxs[,k]
    class_train = labels[trainIdx]
    class_test = labels[-trainIdx]

    train_data = data[trainIdx,]
    test_data = data[-trainIdx,]
    train_df = data.frame('True.label' = as.factor(class_train),'trainIdx'=trainIdx)
    rownames(train_df) = sampleIDs[trainIdx]
    test_df = data.frame('True.label'= as.factor(class_test))
    rownames(test_df) = sampleIDs[-trainIdx]
} else {
    f = sprintf(data_name,k)
    print(paste("Running",f))
    data = load(paste(data_dir,f,sep=''))

    train_data = xtrain_data
    test_data = xtest_data
    class_train = ytrain_data
    class_test = ytest_data 

    train_df = data.frame('True.label' = as.factor(class_train))
    rownames(train_df) = rownames(train_data)
    test_df = data.frame('True.label'= as.factor(class_test))
    rownames(test_df) = rownames(test_data)
    geneIDs = colnames(train_df)

}
train_mean = apply(train_data, 2, mean)
train_sd = apply(train_data, 2, sd)
no_var_idx = which(train_sd==0.0)
# Train
if (length(no_var_idx) > 0) {
    train_data = train_data[,-no_var_idx]
    test_data = test_data[,-no_var_idx]
    train_mean = train_mean[-no_var_idx]
    train_sd = train_sd [-no_var_idx]
}
z_train = sweep(train_data, 2, train_mean, "-")
z_train = sweep(z_train, 2, train_sd, "/")
# Test
z_test = sweep(test_data,2,train_mean,"-")
z_test = sweep(z_test,2,train_sd,"/")

fit_list = list()


nGenes = min(1000,dim(data)[2])
varImps = data.frame()[1:nGenes,]
for (jj in 1:length(subset_methods)) { #length(methods)) {
    m = subset_methods[jj]
    method = methods[[m]]
    print(method_disp[[m]])
    fit = train(z_train,class_train,method=method,trControl = cvCtrl,tuneGrid = tuneGrids[[m]],metric=metric)
     fit_list[[m]] = fit
    
    save_colname = paste(method_disp[[m]],'.pred',sep='')
    train_df[save_colname] = predict(fit,z_train)
    test_df[save_colname] = predict(fit,z_test)

    train_response = predict(fit,z_train,type='prob')
    test_response = predict(fit,z_test,type='prob')

    if (m != 3) {
        miv = varImp(fit)        
        tmp = miv$importance 
        max_score = apply(tmp,1,max)
        mean_score = apply(tmp,1,mean)
        max_ord = rev(order(max_score))
        mean_ord = rev(order(mean_score))
    } else{ # handle SVM feature importance differently 
        tmp = abs(t(fit$finalModel$coefs) %*% fit$finalModel$SV)
        if (nlevels(class_train) == 2) {
           max_ord = rev(order(tmp))
           mean_ord = rev(order(tmp))
           tmp = t(tmp)
           max_score = tmp
           mean_score = tmp
        } else {
	       tmp = t(tmp)
           max_score = apply(tmp,1,max) 
    	   mean_score = apply(tmp,1,mean)
           max_ord = rev(order(max_score))
           mean_ord = rev(order(mean_score))
        }
    }
    varImps[paste(method_disp[[m]],"sortMax",sep='.')] = rownames(tmp)[max_ord[1:nGenes]]
    varImps[paste(method_disp[[m]],"sortMean",sep='.')] =  rownames(tmp)[mean_ord[1:nGenes]]
    varImps[paste(method_disp[[m]],"sortMaxScore",sep='.')] = max_score[max_ord[1:nGenes]]
    varImps[paste(method_disp[[m]],"sortMeanScore",sep='.')] = mean_score[mean_ord[1:nGenes]]
    
    cm = confusionMatrix(train_df[[save_colname]],train_df$True.label,positive=NULL)
    cm_train_tables[[jj]] = cm$table
    roc = auc(multcap(response=train_df$True.label,predicted = data.matrix(train_response)))
    info[count,'dataset'] = save_name
    info[count,'method'] = method_disp[[m]]
    info[count,'datapart'] = 'train'
    info[count,'Accuracy'] = cm$overall['Accuracy']
    info[count,'Bal.Acc'] = cm_metric(cm,"Balanced Accuracy")
    info[count,'AUROC'] = roc
    info[count,'F1'] = cm_metric(cm,"F1")
    info[count,'MCC'] = Matt_Coef(cm)
    count = count+1

    cm = confusionMatrix(test_df[[save_colname]],test_df$True.label,positive=NULL)
    cm_test_tables[[jj]] = cm$table
    roc = auc(multcap(response=test_df$True.label,predicted = data.matrix(test_response)))
    info[count,'dataset'] = save_name
    info[count,'datapart'] = 'test'
    info[count,'method'] = method_disp[[m]]
    info[count,'Accuracy'] = cm$overall['Accuracy']
    info[count,'Bal.Acc'] = cm_metric(cm,"Balanced Accuracy")
    info[count,'AUROC'] = roc
    info[count,'F1'] = cm_metric(cm,"F1")
    info[count,'MCC'] = Matt_Coef(cm)
    count = count + 1 

    train_response$True.label = train_df$True.label
    test_response$True.label = test_df$True.label
    train_response$Pred.label = train_df[[save_colname]]
    test_response$Pred.label = test_df[[save_colname]]
    response_train_wtl_list[[jj]] = train_response
    response_test_wtl_list[[jj]] = test_response

    
}
if (args$saveModel) {
  names(fit_list) = method_disp[subset_methods]
  save_vars = list(train_df,test_df,varImps,fit_list)
  names(save_vars) = c("train_df","test_df","varImps","fit_list")
} else {
  save_vars = list(train_df,test_df,varImps)
  names(save_vars) = c("train_df","test_df","varImps")
}
saveRDS(save_vars,paste(out_dir,f_prefix,save_name,'_resample_',k,'.RDS',sep=''))

response_lists = list(response_train_wtl_list,response_test_wtl_list)
names(response_lists) = c("train","test")
names(response_lists$train) = method_disp[subset_methods]
names(response_lists$test) = method_disp[subset_methods]

cm_tables = list(cm_train_tables,cm_test_tables)
names(cm_tables) = c("train","test")
names(cm_tables$train) = method_disp[subset_methods]
names(cm_tables$test) = method_disp[subset_methods]

lists = list(cm_tables,response_lists,info)
names(lists) = c("cm.tables","response.lists","info")
saveRDS(lists,paste(out_dir,f_prefix,save_name,'_resample_',k,'_cm_responses_info.RDS',sep=''))

#}
