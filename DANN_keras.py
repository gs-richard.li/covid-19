# coding: utf-8
# Deepcode DANN model for WuXi NextCODE AI lab
# Code by Sweta Bajaj, Joseph White, Pengwei Yang, Javier Baylon

# In [ ]:
# Import relevant Libraries and Packages 
import os
import numpy as np
import pandas as pd
from numpy import *
import re
import sys
import collections
import itertools
import random
import argparse
import time
import tensorflow as tf
import keras
from random import shuffle
from sklearn import preprocessing
from operator import itemgetter
from keras import backend as K
from keras.wrappers.scikit_learn import KerasClassifier
from keras.models import Sequential, model_from_json
from keras.layers.core import Dense, Dropout, Flatten
from keras.optimizers import SGD, RMSprop, Nadam
from keras.activations import relu, tanh, softmax, sigmoid
from keras.metrics import mean_squared_error, binary_crossentropy, binary_accuracy
from keras.callbacks import EarlyStopping

from sklearn.preprocessing import LabelEncoder, StandardScaler
from sklearn.metrics import *
from sklearn.model_selection import train_test_split, RandomizedSearchCV
import joblib
import scipy.stats as stats
from six.moves import cPickle as pickle

print(" Start the process : %s" % time.ctime())

seed = 123
random.seed(seed) 
sys.setrecursionlimit(40000)

# Check for the Backend of Keras
print(K.backend()) 
# In [ ]:
# Add the parser arguements
PARSER = argparse.ArgumentParser(description='Program to format the ClinVar data files into a flat file.', usage='%(prog)s [options]', add_help=True)
PARSER.add_argument('-d', '--directory', dest='dataDir', default=None, help='Full path to the data directory')
PARSER.add_argument('-w', '--workdir', dest='workDir', default=None, help='Full path to the working directory')
PARSER.add_argument('-i', '--trainData', dest='trainData', default=None, help='Name of the train data file')
PARSER.add_argument('-t', '--testData', dest='testData', default=None, help='Name of the test data file')
pArgs = PARSER.parse_args()
# In [ ]:
dataDir = os.path.realpath(pArgs.dataDir)
if pArgs.dataDir is None:
        print ("A source data directory is required.")
        exit()
        
# Set output directory to current directory if not set in options.
workDir = os.getcwd() if pArgs.workDir is None else pArgs.workDir
os.makedirs(workDir,exist_ok=True)
os.chdir(workDir)
if pArgs.trainData is None:
        print ("Please supply the name of the training data file")
        exit()
else:
        trainData = pArgs.trainData
        
if pArgs.testData is None:
        print ("Please supply the name of the testing data file")
        exit()
else:
        testData = pArgs.testData
# In [ ]:
# Define all the functions

# load train data
def load_train_data(path):
    df = pd.read_csv(path)
    X = df.values.copy()
    np.random.shuffle(X)
    X, indices, labels  = X[:, 2:].astype(np.float32), X[:, 0].astype(str), X[:, 1].astype(str)
    encoder = LabelEncoder()
    y = encoder.fit_transform(labels).astype(np.int32)
    scaler = StandardScaler()
    X = scaler.fit_transform(X)
    return X, y, encoder, scaler, labels, indices

# load test data    
def load_test_data(path, scaler, encoder):
    df = pd.read_csv(path)
    X = df.values.copy()
    X, indices, labels  = X[:, 2:].astype(np.float32), X[:, 0].astype(str), X[:, 1].astype(str)
    X = scaler.transform(X)
    return X, labels, indices

# Save results    
def save_results(clf, X_test_data, indices_list, labels, encoder, traintest_name = "dataset_name",name='DANN_results.csv'):
    
    #get best keras model:
    best_model = random_search.best_estimator_.model    
    y_prob = best_model.predict_proba(X_test_data)
    y_pred_class = best_model.predict_proba(X_test_data).argmax(axis = -1)
    with open(name, 'w') as f:
        f.write(str(traintest_name) +'_sample_ids,')
        f.write('True_' + str(traintest_name) + '_Class,')
        f.write('Pred_' + str(traintest_name) + '_Class,')
        f.write(','.join([str('Pred_') + str(traintest_name) +  str('_Prob_class_') + str(i) for i in encoder.classes_]))
        f.write('\n')
        for index, label_name, pred_class, probs in zip(indices_list, labels,  y_pred_class, y_prob):
            #this breaks in python 3, uncomment if python 2 is used
            #probas = ','.join([index]+ [label] + map(str, probs.tolist()))
            # this works in python 3, comment out if python 2 is used:
            templist = probs.tolist()
            # insert label:
            templist.insert(0, pred_class)
            templist.insert(0,label_name)
            templist.insert(0,index)
            probas = ",".join(map(str, templist))
            f.write(probas)
            f.write('\n')
#        f.write('Best Params: \n')
#        for k, v in clf.best_params_.items():
#            seq=(str(k), str(v))
#            f.write(','.join(seq))
#            f.write('\n') 
    print("Wrote results to file {}.".format(name))
    
#Get accuracy with test/train set:
def get_metrics(clf, X_test_data, labels_list, encoder):
    #get best keras model:
    best_model = random_search.best_estimator_.model
    
    #get predicted classes:
    y_prob = best_model.predict_proba(X_test_data).argmax(axis = -1)
    y_prob_value =  best_model.predict_proba(X_test_data)
   
    #get actual labels encoded properly!:
    y_test = encoder.fit_transform(labels_list).astype(np.int32)
    
    #get accuracy:
    acc_score = accuracy_score(y_test, y_prob)
    
    # get precision score
#    precision_value = precision_score(y_test, y_prob)
#    
#    # get recall score
#    recall_value = recall_score(y_test, y_prob)
#
#    #get f1_score:
#    f1_score_value = f1_score(y_test, y_prob)
#    
#    #get auc:
#    auc_value = roc_auc_score(y_test, y_prob_value[:,1])
#    
#    true_neg = confusion_matrix(y_test, y_prob).ravel()[0]
#    false_pos = confusion_matrix(y_test, y_prob).ravel()[1]
#    false_neg = confusion_matrix(y_test, y_prob).ravel()[2]
#    true_pos = confusion_matrix(y_test, y_prob).ravel()[3]
    
    print("Accuracy with best model: %4.4f" % acc_score)
#    print("Precision with best model: %4.4f" % precision_value)
#    print("Recall with best model: %4.4f" % recall_value)
#    print("F1 Score with best models: %4.4f" % f1_score_value)
#    print("AUC with best models: %4.4f" % auc_value)
#    return acc_score, precision_value, recall_value, f1_score_value, auc_value, true_neg, false_pos, false_neg,true_pos 
    return acc_score
# In [ ]:
# load train and test datasets
# LOAD REFERENCE DATASET TO BUILD MODEL AUTOMATICALLY!!!
X, y, encoder, scaler, label, index_train = load_train_data(dataDir+'/'+trainData)
X_test, label_test, index_test = load_test_data((dataDir+'/'+testData), scaler, encoder)    
label_test = encoder.transform(label_test)

# Make the Class labels categorical
num_classes = len(encoder.classes_)
num_features = X.shape[1]
y_train = keras.utils.to_categorical(y, num_classes)
y_test = keras.utils.to_categorical(label_test, num_classes)
# In [ ]:
# Create model with create_model function
def create_model(update_learning_rate=0.2,dense0_num_units=1,dense1_num_units=1,dense2_num_units=1,dropout0_p=0.2,dropout1_p=0.2,dropout2_p=0.2):
    model = Sequential()
    model.add(Dense(dense0_num_units, input_shape=(num_features,),  activation='relu', name='dense0'))
    model.add(Dropout(dropout0_p , name='dropout0'))
    model.add(Dense(dense1_num_units, activation='relu', name='dense1'))
    model.add(Dropout(dropout1_p , name='dropout1'))
    model.add(Dense(dense2_num_units, activation='relu', name='dense2'))
    model.add(Dropout(dropout2_p , name='dropout2'))
    model.add(Dense(num_classes, activation='softmax', name='output'))
    nesterov_momentums = SGD(lr=update_learning_rate,momentum=0.9,  nesterov=True)
    model.compile(loss='categorical_crossentropy', optimizer=nesterov_momentums, metrics=['accuracy'])
    print(model.summary())
    return model
# In [ ]:
# Create the neural network structure
model = KerasClassifier(build_fn=create_model, epochs=100, batch_size=32, verbose=1)
param_dist = {"update_learning_rate": list(np.logspace(-3, -1, num = 5, endpoint=True)),
              "dense0_num_units": list(np.arange(200,550,50)),
              "dense1_num_units": list(np.arange(200,550,50)),
              "dense2_num_units": list(np.arange(200,550,50)),
              "dropout0_p": list(np.linspace(0.05, 0.5, num = 5, endpoint = True)),
              "dropout1_p": list(np.linspace(0.05, 0.5, num = 5, endpoint = True)),
              "dropout2_p": list(np.linspace(0.05, 0.5, num = 5, endpoint = True))}


#n_iter searches default=100:
n_iter = 100
seed = 123
#seed = int(random.random()*1000000000)
random.seed(seed)

#Implement Random Search
random_search = RandomizedSearchCV(estimator=model, param_distributions=param_dist,
                                 n_iter = n_iter, scoring = 'accuracy', 
                                 cv=3,random_state = seed, return_train_score = True, n_jobs=6)

random_search.fit(X, y)
print(random_search.best_params_)
# In [ ]:
# Save best model, encoder, scaler
with open('DANN_HP_BestParams.txt', 'w') as f:
    for key in random_search.best_params_.keys():
        f.write("%s,%s\n"%(key,random_search.best_params_[key]))
        
with open('DANN_encoder.pkl', 'wb') as f:
    pickle.dump(encoder, f)
    
with open('DANN_scaler.pkl', 'wb') as f:
    pickle.dump(scaler, f)

best_model = random_search.best_estimator_.model
best_model.save('DANN_model.h5')
print("Saved model to disk")

# Save predicted results
save_results(random_search, X_test, index_test, label_test, encoder, "Test", name=('DANN_testResults.csv'))  
save_results(random_search, X, index_train, label, encoder,"Train", name=('DANN_trainResults.csv'))

#Save the hyperparameters from random search to a pandas csv file:
cv_df = pd.DataFrame.from_dict(random_search.cv_results_)
cv_df.to_csv(("DANN_HP_search_summary.csv"), index = False)

# Check the Model Metrics
#train_acc_score, train_precision, train_recall, train_f1_score, train_auc_value, train_true_neg, train_false_pos, train_false_neg, train_true_pos = get_metrics(random_search, X, label, encoder)
#test_acc_score, test_precision, test_recall, test_f1_score, test_auc_value, test_true_neg, test_false_pos, test_false_neg, test_true_pos = get_metrics(random_search, X_test, label_test, encoder)
train_acc_score = get_metrics(random_search,X,label,encoder)
test_acc_score = get_metrics(random_search,X_test,label_test,encoder)

metric_names = ["Train_Accuracy","Test_Accuracy"]
metrics_values = [train_acc_score, test_acc_score]
tuple_of_metrics = list(zip(metric_names, metrics_values)) 
metrics_df = pd.DataFrame(tuple_of_metrics)
metrics_df = metrics_df.rename(columns={0: "Metric_Name", 1: "Metric_Value"})
metrics_df_pivot = metrics_df.pivot_table( columns ="Metric_Name", values="Metric_Value") 
metrics_df_pivot.to_csv(("DANN_Stats_Summary.csv"), index = False)

with open(("Metrics.txt"), 'w') as f:
#    f.write(','.join(["Train_accuracy (model over traning set - careful!!!)",train_acc_score]))
    f.write(','.join(["Train_Accuracy",str(train_acc_score)]))
    f.write('\n')
#    f.write(','.join(["Train_Precision",str(train_precision)]))
#    f.write('\n')
#    f.write(','.join(["Train_Recall",str(train_recall)]))
#    f.write('\n')
#    f.write(','.join(["Train_F1Score",str(train_f1_score)]))
#    f.write('\n')
#    f.write(','.join(["Train_AUC",str(train_auc_value)]))
#    f.write('\n')
    f.write(','.join(["Test_Accuracy",str(test_acc_score)]))
    f.write('\n')
#    f.write(','.join(["Test_Precision",str(test_precision)]))
#    f.write('\n')
#    f.write(','.join(["Test_Recall",str(test_recall)]))
#    f.write('\n')
#    f.write(','.join(["Test_F1Score",str(test_f1_score)]))
#    f.write('\n')
#    f.write(','.join(["Test_AUC",str(test_auc_value)]))
#    f.write('\n')
f.close()
